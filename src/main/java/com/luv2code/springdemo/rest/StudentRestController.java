package com.luv2code.springdemo.rest;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.luv2code.springdemo.entity.Student;

@RestController
@RequestMapping("/api")
public class StudentRestController {

	private List<Student> theStudents;

	@PostConstruct
	public void loadStudents() {
		theStudents = new ArrayList<>();

		theStudents.add(new Student("pepito1", "chuchu"));
		theStudents.add(new Student("pepito2", "chuchu"));
		theStudents.add(new Student("pepito3", "chuchu"));
	}

//	define a endpoint for get a list of students
	@GetMapping("/students")
	public List<Student> getStudents() {
		return theStudents;
	}

//	define a endpoint for get a student object by id 
	@GetMapping("/students/{studentId}")
	public Student getStudent(@PathVariable int studentId) {
		
		// check studentid against list of students
		if ((studentId >= theStudents.size()) || (studentId < 0)) {
//			throw new StudentNotFoundException("student id not found - " + studentId);
		}

		return theStudents.get(studentId);

	}

}
